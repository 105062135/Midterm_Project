# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. [chat]
    2. [ load message history]
    3. [chat with new user]
    4. [xxx]
* Other functions (add/delete)
    1. [xxx]
    2. [xxx]
    3. [xxx]
    4. [xxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
這個網站是參考網路上的聊天室firebase寫法，然後再去加一些功能，例如:logout、google登入、以及一些CSS的微調。本來想做用facebook登入，但是會一直跳掉找不出原因。然後我也有使用node.js的local host，這樣讓整個過程更加便利，不用每次都要push到gitlab才能看有沒有改對。登入頁面則是用之前lab課所學的forum的登入template稍作修改。其他database的使用也是照著之前所學firbase的運用。
## Security Report (Optional)
